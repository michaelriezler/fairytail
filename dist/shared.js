// events
export const event = { RUN: 'run',
	REMOVE_LISTENER: 'remove_listner',
	ADD_LISTENER: 'add_listener',
	SYNC_DATA: 'sync_data',
	INIT: 'init'
};

export const worker_response = { CHANGES: 'changes',
	QUERY_COMPLETE: 'query_complete'
};