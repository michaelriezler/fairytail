import { event } from '../shared';
import { message_router } from './message_router';
export { message_router };

export function register_worker(a) {
	let { db, functions, imports } = a;
	let msg_router = message_router({ db, functions });

	self.addEventListener('message', function (e) {
		let [flag, msg] = e.data;

		switch (flag) {
			case event.RUN:
				msg_router.run(msg);
				break;

			case event.REMOVE_LISTENER:
				msg_router.on_remove_listener.apply(null, msg);
				break;

			case event.ADD_LISTENER:
				msg_router.on_add_listener.apply(null, msg);
				break;

			case event.SYNC_DATA:
				msg_router.on_sync_data();
				break;

			case event.INIT:
				msg_router.on_init(msg);
				break;

			default:
				console.log('Unknown Message: ', flag, msg);
		}
	});

	self.postMessage(['ready']);
}