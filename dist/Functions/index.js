let register_cb = event => {
	return { run: fn => ({ event, fn }) };
};

let listen = event => register_cb(event);

export default { listen };

export let event = (name, ...args) => ({ name, args });

export let types = () => ({ _type: { allow_null: false, allow_undefined: false },

	create(type) {
		let self = Object.create(this);
		Object.assign(this._type, type);
		return self;
	},

	get_value() {
		return this._type;
	},

	string() {
		return this.create({ type: 'string' });
	},
	number() {
		return this.create({ type: 'number' });
	},
	bool() {
		return this.create({ type: 'bool' });
	},
	array(type) {
		return this.create({ type: 'array', items: type });
	},
	object(type) {
		return this.create({ type: 'object', keys: type });
	},
	start_with(val) {
		return this.create({ start_with: val });
	},
	allow_null() {
		return this.create({ allow_null: true });
	},
	allow_undefined() {
		return this.create({ allow_undefined: true });
	}
});