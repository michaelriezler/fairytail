

export let Event = { create(label, args) {
    let self = Object.create(this);
    self.label = label;
    self.args = args;
    return self;
  },

  to_wire() {
    return [this.label, this.args];
  }

};

export function dispatch(label, ...args) {
  return Event.create(label, args);
}