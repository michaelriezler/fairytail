import { Observable } from 'rxjs/Observable';
import proxy from './proxy';

let default_init = { sync_url: '',
  sync: true
};

let rql = function (script_path, init = {}) {
  let query_proxy = proxy(script_path);
  let config = Object.assign({}, default_init, init);
  query_proxy.init(config);

  return function run_query(event_query) {
    return Observable.create(observer => {
      query_proxy.post_message(event_query, observer);
    });
  };
};

export { rql };
export default rql;