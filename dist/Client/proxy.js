import { event as e, worker_response as w } from '../shared';

import { Term } from 'rxdb/dist/ReQL/Term';
import { Event } from './dispatch';

let listeners = {};

function remove_listener(id) {
  listeners[id].complete();
  delete listeners[id];
}

function add_listener(id, observer) {
  listeners[id] = observer;
}

let next_id = function () {
  let id = 0;
  return function () {
    return id++;
  };
}();

function notify(data) {
  return function (id) {
    let observer = listeners[id];
    observer.next(data);
  };
}

let proxy = function (script_path) {

  let worker = new Worker(script_path);

  worker.addEventListener('message', function (e) {
    let [msg, data] = e.data;

    if (w.CHANGES === msg) {
      let [ids, value] = data;
      ids.forEach(notify(value));
    } else if (w.QUERY_COMPLETE === msg) {
      let [id] = data;
      remove_listener(id);
    }
  });

  window.addEventListener('online', event => {
    worker.postMessage([e.SYNC_DATA]);
  });

  return { post_message(query_event, observer) {

      let unsub = next_id();
      add_listener(unsub, observer);

      let query = (() => {
        if (Event.isPrototypeOf(query_event)) {
          return ['EVENT', query_event.to_wire()];
        }

        if (Term.isPrototypeOf(query_event)) {
          return ['QUERY', JSON.stringify(term_to_wire(query_event))];
        }

        console.log('Type mismatch expected event or query, instead got', query_event);
      })();

      let msg = [unsub, query];

      worker.postMessage([e.RUN, msg]);
    },

    init(config) {
      worker.postMessage([e.INIT, [config]]);
    }
  };
};

function term_to_wire(term) {
  if (null === term) return null;
  return { term: term.term,
    args: term.args,
    opt_args: term.opt_args,
    next: term_to_wire(term.next),
    parent: null
  };
}

export default proxy;