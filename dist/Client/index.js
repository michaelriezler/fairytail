export { rql as connect } from './rql';
export { dispatch } from './dispatch';
export { make_event_dispatcher } from './make_event_dispatcher';

export function get_type({ old_value, new_value }) {
	if (null === old_value && null !== new_value) return 'INSERT';
	if (null === new_value && null !== old_value) return 'DELETE';
	return 'UPDATE';
}