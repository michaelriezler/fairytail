// functions/todo

import fns from 'fairytail/functions'
import * as e from '../events'


exports.insert_todo = fns
	.listen(e.insert_todo)
	.run(function (db, todo) {
		return db.table('todo').insert(todo)
	})


exports.get_todo = fns
	.listen(e.get_todo)
	.run(function (db, id) {
		return db.table('todo').get(id)
	})


exports.set_done = fns
	.listen(e.)


// functions/event

import { event, types as t } from 'fairytail/functions'


let Todo = t().object(
	{ id : t().uuid()
	, title : t().string()
	, done : t().bool().start_with(false)
	}
)


exports.insert_todo = event('insert_todo', Todo)
exports.get_todo = event(t().uuid())
exports.set_done = event(t().uuid())


// client/worker.js

import * as todo_fns from '../Functions/todo'
import db from 'fairytail/rxdb'
import registerWorker from 'fairytail/worker'


db.tableCreate('todos', {/*config*/})

registerWorker(
	{ db
	, functions : [ todo_fns ]
	/* 
		Dynamically import scripts based on events
		The functions of the imported script will be registerd
	*/
	, imports :
			{ 'event' : [ 'script_one']
			, 'other_event' : ['other_script', 'another_one']
			}
	}
)