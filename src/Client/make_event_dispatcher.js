import { rql as connect } from './rql'
import { dispatch } from './dispatch'


export function make_event_dispatcher (events, run) {

	let dispatch_fn = name => (...args) => run(dispatch(name, args))

	let dispatch = Object
		.values(events)
		.map(({ name }) => name)
		.map(name => ({ [name] : dispatch_fn(name) }))
		.reduce((acc, x) => Object.assign(acc, x), {})


	return { dispatch, fairytail : f }
}