// @flow

import { Observable } from 'rxjs/Observable'
import proxy from './proxy'


export type Query = [string, Array<any>];




let default_init =
  { sync_url : ''
  , sync : true 
  }


type Init = {
  sync_url? : string,
  sync? : boolean,
};


let rql = function (script_path : string, init : Init = {}) {
  let query_proxy = proxy(script_path)
  let config = Object.assign({}, default_init, init)
  query_proxy.init(config) 

  return function run_query (event_query) {
    return Observable.create(observer => {
      query_proxy.post_message(event_query, observer)
    })
  }
}


export { rql }
export default rql