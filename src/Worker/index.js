// @flow

import { event } from '../shared';	
import { message_router } from './message_router';
export { message_router };

import type { Command } from './message_router';
import type { MessageType } from '../shared';
export type Message = Command;
export type Letter = [MessageType, string];
export type MessageEvent = { data: Letter };

export function register_worker (a: { db: any, functions: Array<any>, imports?: any }) {
	let { db , functions, imports } = a
	let msg_router = message_router({ db, functions })

	self.addEventListener('message', function (e: MessageEvent) {
		let [flag, msg] = e.data

		switch (flag) {
			case event.RUN :
				msg_router.run(msg)
				break

			case event.REMOVE_LISTENER :
				msg_router.on_remove_listener.apply(null, msg)
				break

			case event.ADD_LISTENER :
				msg_router.on_add_listener.apply(null, msg)
				break

			case event.SYNC_DATA :
				msg_router.on_sync_data()
				break

			case event.INIT :
				msg_router.on_init(msg)
				break

			default:
				console.log('Unknown Message: ', flag, 	msg)
		}
	})

	self.postMessage(['ready'])
}