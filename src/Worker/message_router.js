// @flow

import { query, table, get, update } from 'rxdb/dist/ReQL/Functions'
import { run, table_create  } from 'rxdb/dist/Driver/Web'
import { worker_response as wr } from '../shared'
import uuid from 'uuid/v4'
import type { EventCallback } from '../Functions'


export type Command = Array<any>;

let global_config = {}
let routes : { [string] : Array<{ fn (any) : any, args : Array<any> }> } = {}
let observer = {}
let map_id_table : { [number] : string } = {}


let get_observer = (table : string) =>
	(observer[table] || [])


let remove_obsever = (id : number) => {
	if (undefined === map_id_table[id]) return 

	let table = map_id_table[id]
	observer[table] = observer[table].filter(item => item !== id)
	delete map_id_table[id]
}


let push_observer = (observer_id : number, table : string) => {
	if (undefined === observer[table])
		observer[table] = []

	observer[table].push(observer_id)
	map_id_table[observer_id] = table
}	


query :: table_create('$$fairytail-eventlog$$', { indexes : ['date', 'sync'] })


let sync_success = eid => query
	:: table ('$$fairytail-eventlog$$')
	:: get (eid)
	:: update (event => ({ sync : true }) )


let insert_event = function (payload : { event_id ?: string, date ?: any, msg : any }) {
		let event_id : string = uuid()
		let date = new Date().toUTCString()

		Object.assign(payload, { event_id, date })
		event_table.insert(payload).subscribe()

		fetch(global_config.sync_url, { method : 'POST', payload })
			.then(_ => run(sync_success(event_id)).subscribe())
}


let on_query_success = (o_id : number) => data => {
	notify([o_id], data)
}


let on_query_error  = err => 
	self.postMessage(['error', err])


let notify = (ids = [], data) => {
	if (0 < ids.length) {
		self.postMessage([wr.CHANGES, [ids, data]])
	}
}


let query_complete = id => () => {
	self.postMessage([wr.QUERY_COMPLETE, [id]])
}


let validate = (schema, args) => {
	return schema.map((def, i) => {
		// TODO : proper validation
		return args[i]
	})
}


let register_route = (routes, { event, fn }) => {
	if (undefined === routes[event.name])
		routes[event.name] = []

	routes[event.name].push({ fn, args : event.args })
}


let exec_query = o_id => fn => run(fn)
	.subscribe(on_query_success(o_id), on_query_error, query_complete(o_id))


let make_query = (msg, type, db) => cb => {
	let { args : fn_args, fn } = cb
	let args = validate(fn_args, msg)
	return fn.apply(null, [db].concat(args))
}


export let message_router = function (o : { config ?: any, db : any, functions : Array<Array<EventCallback>> }) {
	let { config, db, functions } = o
	Object.assign(global_config, config)

  functions.forEach((fns : Array<EventCallback>) => {
  	for (let key in fns) {
  		if (fns.hasOwnProperty(key)){
  			register_route(routes, fns[key])
  		}
  	}
  })


	let router =
		{ run (msg : Command) {
				let [observer_id, [type, message]] = msg

				let querys = (() => {
					if ('EVENT' === type) {
						//insert_event([type, args])
						let [label, args] = message
						return (routes[label] || []).map(make_query(args, type, db))
					}

					if ('QUERY' === type) {
						return [JSON.parse(message)]
					}
				})()

				querys.forEach(exec_query(observer_id))
			}

		, on_remove_listener : remove_obsever
		, on_add_listener : push_observer

		, on_sync_data () {
				event_table.filter({ sync : false }).subscribe(payload => {
					fetch(global_config.sync_url, { method: 'POST', payload })
						.then(_ => sync_success(payload.event_id))
				})
			}

		, on_init (config : any) {}

		}

	return router
}