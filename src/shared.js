// events
export const event = 
	{ RUN : 'run'
	, REMOVE_LISTENER : 'remove_listner'
	, ADD_LISTENER : 'add_listener'
	, SYNC_DATA : 'sync_data'
	, INIT : 'init'
	}

export type MessageType = $Keys<typeof event>;


export const worker_response =
	{ CHANGES : 'changes'
	, QUERY_COMPLETE : 'query_complete'
	}

export type WorkerResponse = $Keys<typeof worker_response>