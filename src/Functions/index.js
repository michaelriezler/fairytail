// @flow

type Event = {
	name : string,
	args : Array<Types>
};

type Types = {
	allow_null : boolean,
	allow_undefined : boolean,
	type : string,
};

export type EventCallback = { event : Event, fn : (any) => any };

type Runable = {
	run((any) => any) : EventCallback
};


let register_cb = (event : Event) : Runable => {
	return { run : fn => ({ event, fn }) }
}

let listen = (event : Event) => register_cb(event)

export default { listen }

export let event = (name : string, ...args : Array<Types>) : Event => ({ name, args })


type TypeDef 
	= { type : string, items ?: any }
	| { start_with : any }
	| { allow_null : boolean }
	| { allow_undefined : boolean }
	;

export let types = () => (
	{ _type : { allow_null : false, allow_undefined : false }

	, create (type : TypeDef) {
			let self = Object.create(this)
			Object.assign(this._type, type)
			return self
		}

	, get_value () { return this._type }

	, string () { return this.create({ type : 'string' })}
	, number () { return this.create({ type : 'number' })}
	, bool () { return this.create({ type : 'bool' }) }
	, array (type : TypeDef) { return this.create({ type : 'array', items : type }) }
	, object (type : TypeDef) { return this.create({ type : 'object', keys : type }) }
	, start_with (val : any) { return this.create({ start_with : val }) }
	, allow_null () { return this.create({ allow_null : true }) }
	, allow_undefined () { return this.create({ allow_undefined : true }) }
	}
)