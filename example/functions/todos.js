// Here we define which queries should run in response to an event.
// The goal here is to reuse the queries for the client and the server.

import fns from '../../dist/Functions'
import * as e from '../events'

import { get, table, coerce_to, filter, row, eq, _delete, insert 
			 , update, pluck
			 } from 'rxdb/dist/ReQL/Functions'


export let insert_todo = fns
	// we start by listening to an event
	.listen(e.insert_todo)
	// the first argument to the callback is the db connection, followed by
	// by the rest of the arguments
	.run(function (db, title) {
		let todo = Object.assign({ title }, { body : '', done : false })
		return db 
			:: table ('todos')
			:: insert (todo)
	})


let get_todo = (db, id) => db
	:: table ('todos')
	:: get (id)


export let get_todo_ = fns
	.listen(e.get_todo)
	.run(function (db, id) {
		return get_todo (db, id)
	})


export let get_todos = fns
	.listen(e.get_todos)
	.run(function (db) {
		return db :: table ('todos') :: pluck ('id', 'title')
	})


export let get_active_todo = fns
	.listen(e.get_active_todo)
	.run(function (db, id) {
		return db :: table ('todos') :: get (id)
	})


export let active = fns
	.listen(e.get_active_todos)
	.run(function (db) {
		return db 
			:: table ('todos') :: filter (row ('done') :: eq (false))
	})


let get_completed_todos = db => db 
	:: table ('todos') :: filter (row ('done') :: eq (true))


export let completed = fns
	.listen(e.get_completed_todos)
	.run(function (db) {
		return get_completed_todos (db)
	})


export let delete_completed = fns
	.listen(e.delete_completed)
	.run(function (db) {
		return get_completed_todos (db) :: _delete ()
	})


export let set_done = fns
	.listen(e.set_done)
	.run(function (db, id, done) {
		return db 
			:: table ('todos')
			:: get (id)
			:: update ({ done })
	})


export let edit = fns
	.listen(e.edit_todo)
	.run(function (db, id, title) {
		return get_todo (db, id) :: update ({ title })
	})


export let delete_ = fns
	.listen(e.delete_todo)
	.run(function (db, id) {
		return get_todo (db, id) :: _delete ()
	})


export let update_body = fns
	.listen(e.update_body)
	.run(function (db, id, body) {
		return db :: table ('todos') :: get (id) :: update ({ body })
	})