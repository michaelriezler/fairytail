let path = require('path')
let HTMLPlugin = require('html-webpack-plugin')
let ExtractTextPlugin = require('extract-text-webpack-plugin')
let dist = path.join(__dirname, 'dist')


let client = 
	{ devtool : 'inline-source-map'
	, entry : ['babel-polyfill', './client/index.js']
	, output :
			{ path : dist
			, filename : 'client.bundle.js'
			}

	, module :
			{ rules :
					[ { test : /\.jsx?$/
						, loader : 'babel-loader'
						, exclude: /node_modules/
						}

        	, { test: /\.css$/
        		, exclude: /node_modules/
        		, use: ExtractTextPlugin.extract(
        				{ fallback: 'style-loader'
          			, use: ['css-loader']
        				})
      			}
					]
			}

	, plugins :
			[ new HTMLPlugin(
					{ template : './client/index.html' 
					, filename : 'index.html'
					, inject : 'body'
					}
				)
			, new ExtractTextPlugin(
					{ filename: './styles/style.css'
					, disable: false
					}
				)
			]

	, resolve :
			{ modules : [ 'node_modules', path.join(__dirname, "node_modules") ]
			}
	}


let worker = 
	{ devtool : 'inline-source-map'
	, entry : ['babel-polyfill', './client/worker.js']
	, target : 'webworker'
	, output : 
			{ filename : 'worker.js'
			, path : dist
			}
	, module :
		{ rules :
				[ { test : /\.jsx?$/
					, loader : 'babel-loader'
					, exclude: /node_modules/
					}
				]
		}

	, resolve :
			{ modules : [ 'node_modules', path.join(__dirname, "node_modules") ]
			}
	}


module.exports = [client, worker]