import { register_worker } from '../../dist/Worker'
import { query as db } from 'rxdb/dist/ReQL/Functions'
import { table_create } from 'rxdb/dist/Driver/Web'
import * as todo_fns from '../functions/todos'

// before we can make use of a table, we first need to create on
// the prefix will come in handy for react native and async storage.
db :: table_create('todos', { prefix : '@ft::', indexes : ['id', 'done'] })

register_worker({ db, functions : [todo_fns] })