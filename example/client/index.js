import './style.css'
import React, { Component } from 'react'
import ReactDOM from 'react-dom'
import { connect, dispatch, get_type } from '../../dist/Client'
import { query, table, changes, get } from 'rxdb/dist/ReQL/Functions'


// connect will spawn a new web worker (worker.js) and is responsible to handle
// messages between the worker and the main thread.
let run = connect ('worker.js')


// next we are going to prepare the events that we want to send to the worker.
// you can think of this like action creators in redux.
// run will return an observable and will only send the event/query when you
// subscribe to it.

let delete_todo = id => () =>
	// we use dispatch to indicate that we are going to send an event to the worker
	run(dispatch('delete_todo', id)).subscribe((res) => {
		console.log('Todo deletetd: ', id)
	})


let get_todos = 
	run(dispatch('get_todos'))


let insert_todo = todo =>
	run(dispatch('insert_todo', todo))


let update_body = (id, value) =>
	run(dispatch('update_body', id, value))


let get_active_todo = id =>
	run(dispatch('get_active_todo', id))


let update_title = (id, title) =>
	run(dispatch('edit_todo', id, title))


// we can also prepare reql queries and send them to the worker
let todo_changefeed =
	run(query :: table('todos') :: changes())



// just some stateless components

let TodoItem = props => 
	<span onClick={props.onClick} className="todo-item">
		{ props.title }
		<button onClick={delete_todo(props.id)}>X</button>	
	</span>


let TodoForm = props =>
	<div className="todo-form">
		<form onSubmit={props.onSave}>
			<input 
				onChange={props.onChange} 
				name="todo" 
				type="text"
				placeholder="Create new Todo"
				value={props.value}
			/>
		</form>
	</div>




class App extends Component {
	
	constructor () {
		super()

		this.state =
			{ todos : []
			, new_todo : ''
			, active : ''
			}
	}

	render() {
		let { state } = this
		return (
			<div className="container">
				<div className="todos">
					<h1>Fairytail Todo Example</h1>

					<TodoForm
						onSave={this.insert_todo.bind(this)}
						onChange={e => this.setState({ new_todo : e.target.value})} 
						value={this.state.new_todo}
					/>

					<ul>
						{ state.todos.map(todo => 
								<li key={todo.id}>
									<TodoItem
										title={todo.title}
										id={todo.id}
										onClick={_ => this.setState({ active : todo.id })}
									/>
								</li>) 
						}
					</ul>
				</div>

				<ActiveTodo todo={state.active} />

			</div>
		)
	}

	// the change object contains the old_value and the new_value
	handle_todos (changes) {
		let { todos } = this.state
		switch (get_type(changes)) {
			
			// old_value = null
			// new_value = {}
			case 'INSERT' :
				todos.push(changes.new_value)
				this.setState({ todos })
				break

			// old_value = {}
			// new_value = {}
			case 'UPDATE' :
				todos = todos.map(todo => {
					if (todo.id === changes.new_value.id) {
						return changes.new_value
					}
					return todo
				})
				this.setState({ todos })
				break

			// old_value = {}
			// new_value = null
			case 'DELETE' :
				todos = todos.filter(todo => todo.id !== changes.old_value.id)
				this.setState({ todos })
				break

			// this should never happen
			default :
				console.log(type, value)
		}
	}

	insert_todo (e) {
		e.preventDefault()
		insert_todo(this.state.new_todo).subscribe(x => console.log('Todo inserted', x))
		this.setState({ new_todo : '' })
	}

	componentWillMount () {
		// the changefeed observable never completes. The onNext method will be called
		// everytime we have a change (insert, update, delete) inside the database.
		todo_changefeed.subscribe(this.handle_todos.bind(this))
	}

	async componentDidMount() {
		get_todos.subscribe(todo => {
			this.state.todos.push(todo)
			this.setState({ todos : this.state.todos })
		})
	}

	
}


class ActiveTodo extends Component {
	constructor() {
		super()

		this.state =
			{ body : ''
			, title : ''
			, edit_title : false
			, new_title : ''
			, id : ''
			}
	}

	render () {
		let { state } = this
		return (
			<div className="todo-detail">
				{(() => {
					if (state.edit_title) {
						return <form onSubmit={this.save_title.bind(this)}>
							<input 
								type="text" 
								defaultValue={state.title}
								onChange={e => this.setState({ new_title : e.target.value })}
								onBlur={this.save_title.bind(this)}
							/>
						</form>
					} 

					return <h2
						onClick={() => this.setState({ edit_title : true })}
					>{ state.title }</h2>
				})()}
				<div className="todo-detail-wrapper">
					<button>Add Due Date</button>
					<button>Add Reminder</button>
				</div>
				<textarea
					value={state.body}
					onChange={e => this.setState({ body : e.target.value })}
					onBlur={e => update_body(state.id, e.target.value).subscribe() }
				></textarea>
			</div>
		)
	}

	componentWillReceiveProps({ todo }) {
		// get a fresh todo everytime the id changes
		get_active_todo(todo).subscribe(data => {
			console.log(data)
			this.setState(data)
		})
	}

	save_title () {
		update_title(this.state.id, this.state.new_title).subscribe()
		this.setState({ new_todo : '', edit_title : false })
	}
}


ReactDOM.render(<App />, document.querySelector('#root'))