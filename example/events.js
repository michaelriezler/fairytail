import { event, types as t } from '../dist/Functions'


// define the events we want to send to the web worker / server
// the definition takes a label, followed by the types of the arguments

export let get_todo 						= event('get_todo', t().string())
export let get_todos 						= event('get_todos')
export let insert_todo 					= event('insert_todo', t().string())
export let get_active_todos 		= event('get_active_todos')
export let get_completed_todos 	= event('get_completed_todos')
export let delete_completed 		= event('delete_completed')
export let set_done 						= event('set_done', t().string(), t().bool())
export let edit_todo 						= event('edit_todo', t().string(), t().string())
export let delete_todo 					= event('delete_todo', t().string())
export let get_active_todo 			= event('get_active_todo', t().string())
export let update_body 					= event('update_body', t().string(), t().string())